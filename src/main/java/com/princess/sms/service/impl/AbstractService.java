package com.princess.sms.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.princess.sms.service.Service;

/**
 * Base class for Business Services - use this class for utility methods and generic CRUD methods.
 * 
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
public abstract class AbstractService
    implements Service {
  private static final Long DEFAULT_LONG = null;
  private static final Logger LOGGER = LoggerFactory.getLogger(AbstractService.class);

  static {
    LOGGER.info("Registering converters");
  }

  protected AbstractService() {
  }

}
