package com.princess.sms.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.princess.sms.ExceptionCode;
import com.princess.sms.exception.SmsException;
import com.princess.sms.model.Voyage;
import com.princess.sms.service.VoyageService;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
@Service("voyageService")
public class VoyageServiceImpl extends AbstractService
    implements VoyageService {

  private static final Logger LOGGER = LoggerFactory.getLogger(VoyageServiceImpl.class);

  @Autowired
  RestTemplate restTemplate;

  @Value("${shipstar.ws.url}")
  private String shipstarWSURL;

  /**
   * Method findCurrentVoyage.
   * 
   * 
   * 
   * 
   * @return Voyage * @see com.princess.shipstar.service.VoyageService#findCurrentVoyage() * @see
   *         com.princess.shipstar.service.VoyageService#findCurrentVoyage()
   */
  @Override
  public Voyage findCurrentVoyage() {
    StringBuilder wsUrl = new StringBuilder(shipstarWSURL).append("voyages/current");

    try {
      ResponseEntity<Voyage> response = restTemplate.getForEntity(wsUrl.toString(), Voyage.class);
      return response.getBody();
    } catch (RestClientException e) {
      LOGGER.warn("Error retrieving current voyage : {}", wsUrl.toString(), e);
      throw new SmsException(ExceptionCode.CURRENT_VOYAGE_NOT_FOUND, e);
    }
  }

}
