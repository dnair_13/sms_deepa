package com.princess.sms.webapp.controller;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.princess.sms.model.Ship;
import com.princess.sms.model.Voyage;
import com.princess.sms.service.VoyageService;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
@WebAppConfiguration
public class VoyageControllerTest {
  private static final Logger LOGGER = LoggerFactory.getLogger(VoyageControllerTest.class);

  @InjectMocks
  private VoyageController voyageController;

  private MockMvc mvc;

  @Mock
  private VoyageService voyageService;


  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);

    mvc = MockMvcBuilders.standaloneSetup(voyageController).build();
  }

  @Test
  public void testFindCurrentVoyage() throws Exception {
    Ship ship = new Ship();
    ship.setCode("SA");
    Voyage voyage = new Voyage();
    voyage.setShip(ship);
    voyage.setVoyageNumber("H446");

    Mockito.when(voyageService.findCurrentVoyage()).thenReturn(voyage);
    mvc.perform(MockMvcRequestBuilders.get("/voyages/current").accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andExpect(content().string(equalTo(
            "{\"createdBy\":null,\"dateCreated\":null,\"dateModified\":null,\"modifiedBy\":null,\"ship\":{\"createdBy\":null,\"dateCreated\":null,\"dateModified\":null,\"modifiedBy\":null,\"code\":\"SA\",\"name\":null},\"voyageNumber\":\"H446\"}")));

  }
}
